Code for CUDA filterbank kernel as used in DAFX-19 paper.
More recent versions may exist, look at other repos or GitHub.

TODO(travis): Before camera-ready, consider just pointing everyone
to a more modern and evolving repository. Then we can apply a tag
to mark the short version used for DAFx.
