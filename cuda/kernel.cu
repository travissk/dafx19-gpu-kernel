#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cuComplex.h"
#include <stdio.h>

#ifdef WINDOWS
#include <windows.h>
#include <tchar.h>
#else
#include <errno.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#endif


#ifdef WINDOWS
TCHAR szName[] = TEXT("Local\\GPUModalBankMem");
TCHAR szNameSemaphore[] = TEXT("Local\\GPUModalBankSemaphore");
TCHAR szNameSemaphoreGPU[] = TEXT("Local\\GPUModalBankSemaphoreGPU");
#else
#define SEMNAMECPU "/sem_lac_cpu"
#define SEMNAMEGPU "/sem_lac_gpu"
sem_t* sem_cpu;
sem_t* sem_gpu;
int shm_fd;
#endif

#define SHAREDMEMSIZE 1024*512

#define BUFFERSIZE 256
constexpr int NDRUMS = 10;
constexpr int NMODES = NDRUMS * 1024;
constexpr int MODES_PER_DRUM = 1000;
constexpr int NWARPS = 320;  // NMODES / (32 threads per warp)

static float host_samplebuffer[BUFFERSIZE*NWARPS];

// SHARED MEMORY LAYOUT
// ----- FIRST HALF -----
// 10000 modes * ModeInfo:
struct ModeInfo {
	bool enabled;
	bool reset;

	bool amp_changed;
	float amp_real;
	float amp_imag;
	
	float damp;
	float freq;
	bool freq_changed;
};  // approximately 8 * 4 bytes = 32 bytes
// all modes total = 320000 = 320KB
// and NDRUMS times BUFFERSIZE for inputs. = 10240 = 10KB
// ----- SECOND HALF -----
// audio output to host.
// 4 bytes * 1024 buffer size = 4K


// The CUDA library provides all complex math we need aside from cexpf.
__device__ __forceinline__ cuFloatComplex custom_cexpf(cuFloatComplex z) {
	cuComplex res;
	float t = expf(z.x);
	sincosf(z.y, &res.y, &res.x);
	res.x *= t;
	res.y *= t;
	return res;
}

__global__ void filterbankKernel(
    float* yprev,        // Previous state
    const ModeInfo* mi,  // Mode frequencies and dampings.
    const float* input,  // Input signal.
    float* output) {     // Buffer we write output samples to.
  // Initialization - each thread figures out its
  // place in the world (i.e., it's the ith of N threads).
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int which_warp = (int)(i / 32);
	bool is_first_thread_in_warp = (i % 32) == 0;

	// Load prior state from global memory.
	cuComplex y;
	y.x = yprev[2 * i];
	y.y = yprev[2 * i + 1];
  // If prior state should be discarded, zero it.
	if (mi[i].reset) {
		y.x = 0.0f;
		y.y = 0.0f;
	}

  // Introduce variables to make expressions more readable later.
	cuComplex input_amp;
	input_amp.x = mi[i].amp_real;
	input_amp.y = mi[i].amp_real;
	cuComplex input_complex;
	cuComplex exp_term;
	int which_drum = (int)(i / MODES_PER_DRUM);
  const float *input_base = input + (BUFFERSIZE*which_drum);

  // Regenerate the exponential term at the start of each buffer.
  // This is moved inside the loop in case of parameter interpolation,
  // modifying params on zero-crossings, or similar cases.
  cuComplex e_term_tmp;
  e_term_tmp.x = -mi[i].damp;
  e_term_tmp.y = mi[i].freq;
  exp_term = custom_cexpf(e_term_tmp);
  
	// Main loop - run enough cycles to generate the whole buffer.
	for (int samp = 0; samp < BUFFERSIZE; samp++) {
		y = cuCmulf(exp_term, y);
    // We always compute input for now, but could skip this multiply
    // if we know there's no input.
		input_complex.x = input_base[samp];
		input_complex.y = 0.0f;
		y = cuCaddf(y, cuCmulf(input_complex, input_amp));

    // Merge audio across all threads in the warp (32 threads),
    // and store the sample in our output buffer.
    // This sums in parallel, and completes in log_2(32) = 5 steps.
		// We use the real part of the complex sample as output audio.
		float merge_output = y.x;
		for (int offset = 16; offset > 0; offset /= 2)
			merge_output += __shfl_down_sync(0xffffffff, merge_output, offset);
		if (is_first_thread_in_warp) {
			output[which_warp*BUFFERSIZE + samp] = merge_output;
    }
  }

  // Save state back to shared memory, so we may restore it
  // on the next kernel launch.
	yprev[2 * i] = y.x;
	yprev[2 * i + 1] = y.y;
}

int main()
{
#ifdef WINDOWS
	HANDLE hMapFile = CreateFileMapping(
		INVALID_HANDLE_VALUE, // use paging file,
		NULL, // default security
		PAGE_READWRITE,
		0, // max objeect size (high-order)
		SHAREDMEMSIZE,  // max obj size (low-order),
		szName);
	if (hMapFile == nullptr) {
		fprintf(stderr, "shared memory init failed! %d", GetLastError());
		return 1;
	}
	LPCTSTR pBuf = (LPTSTR)MapViewOfFile(hMapFile,
		FILE_MAP_ALL_ACCESS,
		0,
		0,
		SHAREDMEMSIZE);
	if (pBuf == nullptr) {
		fprintf(stderr, "mapviewoffile failed! %d", GetLastError());
		CloseHandle(hMapFile);
		return 1;
	}

	HANDLE hSemaphore = CreateSemaphoreA(NULL, 0, 1, szNameSemaphore);
	if (hSemaphore == nullptr) {
		fprintf(stderr, "could not create semaphore %d", GetLastError());
		CloseHandle(hMapFile);
		return 1;
	}
	HANDLE hSemaphoreGPU = CreateSemaphoreA(NULL, 0, 1, szNameSemaphoreGPU);
	if (hSemaphore == nullptr) {
		fprintf(stderr, "could not create semaphore-gpu %d", GetLastError());
		CloseHandle(hMapFile);
		return 1;
	}
#else
	// Create and map a block of shared memory
	shm_fd = shm_open("/shm_filterbank", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	if (!shm_fd) {
		fprintf(stderr, "could not create shared memory %d", errno);
		return 1;
	}
	ftruncate(shm_fd, SHAREDMEMSIZE);
	void *pBuf = mmap(NULL, SHAREDMEMSIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
	
	// Create two cross-process semaphores
	sem_cpu = sem_open(SEMNAMECPU, O_CREAT, 0644, 0);  // Initial value 0
	if (sem_cpu == SEM_FAILED) {
		fprintf(stderr, "could not create semaphore-cpu %d", errno);
		return 1;
	}
	sem_gpu = sem_open(SEMNAMEGPU, O_CREAT, 0644, 0);  // Initial value 0
	if (sem_gpu == SEM_FAILED) {
		fprintf(stderr, "could not create semaphore-gpu %d", errno);
		return 1;
	}
#endif

	// Init all our buffers
	cudaError_t cudaStatus;
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return 1;
	}

	float* dev_previousvalues; // previous values of exponential across kernel launches. Interleaved complex.
	int* dev_modeinfo;  // modeinfo, per-drum.
	float* dev_inputs;  // input signals, per-drum
	float* dev_output_samps;  // output samples, per-warp

	
	cudaStatus = cudaMalloc((void**)&dev_previousvalues, NMODES * 2 * sizeof(float));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc dev_previousvalues failed!");
		return 1;
	}
	cudaStatus = cudaMalloc((void**)&dev_modeinfo, NMODES * sizeof(ModeInfo));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc dev_modeinfo failed!");
		return 1;
	}
	cudaStatus = cudaMalloc((void**)&dev_inputs, NDRUMS * BUFFERSIZE * sizeof(float));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc dev_inputs failed!");
		return 1;
	}
	cudaStatus = cudaMalloc((void**)&dev_output_samps, NWARPS * BUFFERSIZE * sizeof(float));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc output_samps failed!");
		return 1;
	}

	// Obtain relevant pointers into shared memory.
	int* sharedmem_modeinfoptr = (int*)pBuf;
	int* sharedmem_inputptr = (int*)((char*)sharedmem_modeinfoptr + NMODES * sizeof(ModeInfo));
	int* sharedmem_outputptr = (int*)((char*)sharedmem_inputptr + NDRUMS * BUFFERSIZE * sizeof(float));

	fprintf(stderr, "starting main loop...\n");
	while (true) {
#ifdef WINDOWS
		WaitForSingleObject(hSemaphore, INFINITE);
#else
		sem_wait(sem_cpu);
#endif

		// Copy modes from shared memory to device.
		cudaStatus = cudaMemcpy(dev_modeinfo, sharedmem_modeinfoptr, NMODES * sizeof(ModeInfo), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy modeinfos failed!");
			return 1;
		}
		cudaStatus = cudaMemcpy(dev_inputs, sharedmem_inputptr, NDRUMS * BUFFERSIZE * sizeof(float), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy inputs failed!");
			return 1;
		}

		// Kernel launch
		// NMODES total. (10 drums * 1024)
		filterbankKernel << <10, 1024>> > (dev_previousvalues, (ModeInfo*)dev_modeinfo, dev_inputs, dev_output_samps);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			return 1;
		}

		// cudaDeviceSynchronize waits for the kernel to finish, and returns
		// any errors encountered during the launch.
		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching kernel!\n", cudaStatus);
			return 1;
		}
		
		// Copy output vector from GPU buffer to host memory.
		cudaStatus = cudaMemcpy(host_samplebuffer, dev_output_samps, BUFFERSIZE*NWARPS*sizeof(float), cudaMemcpyDeviceToHost);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy samples-back failed!");
			return 1;
		}

		// Sum up and output to buffer
		float* sampsBuf = ((float*)sharedmem_outputptr);
		for (int samplei = 0; samplei < BUFFERSIZE; samplei++) {
			float sample = 0.0f;
			for (int j = 0; j < NWARPS; j++) {
				sample += host_samplebuffer[j*BUFFERSIZE + samplei];
			}
			sampsBuf[samplei] = sample;
		}

#ifdef WINDOWS
		ReleaseSemaphore(hSemaphoreGPU, 1, NULL);
#else
		sem_post(sem_gpu);
#endif
	}

	// TODO: catch a signal to exit the loop and reenable this.
	// cudaDeviceReset();
	// return 0;
}
